require 'rails_helper'

describe Contact, :type => :model do
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to allow_value('Mega Contact Name', 'Trololo').for(:name) }
  it { is_expected.to belong_to(:company) }
  it { is_expected.to have_many(:friendships) }
  it { is_expected.to have_many(:friends).through(:friendships) }

  context 'friendships' do
    let!(:empire) { Company.create(name: 'Death Star') }
    let!(:rebel) { Company.create(name: 'Corellia') }

    let!(:stormtrooper1) { empire.contacts.create(name: 'Stormtrooper 1') }
    let!(:stormtrooper2) { empire.contacts.create(name: 'Stormtrooper 2') }

    let!(:rebeltrooper) { rebel.contacts.create(name: 'Rebel trooper') }

    context 'add' do
      it 'can add other contacts to friends if they belongs to the same company' do
        stormtrooper1.friendships.create(friend_id: stormtrooper2.id)
        expect(stormtrooper1.friends).to eq [stormtrooper2]
      end

      it 'can not add other contacts to friends if they belongs to a different company' do
        friendship = stormtrooper1.friendships.build(friend_id: rebeltrooper.id)
        friendship.save

        expect(stormtrooper1.friends).to be_blank
        expect(friendship.errors[:base]).to include('A friendship can not be established because the contact belongs to the different company')
      end
    end

    context 'remove' do
      before { stormtrooper1.friendships.create!(friend_id: stormtrooper2.id) }
      let(:friendship) { stormtrooper1.friendships.first }

      it 'can remove the contact from friends' do
        friendship.destroy!
        stormtrooper1.friendships.reload
        expect(stormtrooper1.friendships).to be_blank
      end
    end
  end

  context '#friend_with?' do
    let!(:empire) { Company.create(name: 'Death Star') }
    let!(:stormtrooper1) { empire.contacts.create(name: 'Stormtrooper 1') }
    let!(:stormtrooper2) { empire.contacts.create(name: 'Stormtrooper 2') }
    let!(:stormtrooper3) { empire.contacts.create(name: 'Stormtrooper 3') }

    before { stormtrooper1.friendships.create(friend_id: stormtrooper2.id) }

    it 'returns true if contacts are friends' do
      expect(stormtrooper1.friend_with?(stormtrooper2)).to be true
    end

    it 'returns false if contacts are not friends' do
      expect(stormtrooper1.friend_with?(stormtrooper3)).to be false
    end
  end
end