require 'rails_helper'

describe Company, :type => :model do
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to allow_value('Mega Company Name', 'Trololo').for(:name) }
  it { is_expected.to have_many(:contacts) }
end