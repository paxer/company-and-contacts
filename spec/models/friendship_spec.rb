require 'rails_helper'

describe Friendship, :type => :model do
  it { is_expected.to belong_to(:contact) }
  it { is_expected.to belong_to(:friend).class_name('Contact') }
end