class AddContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :name, null: false
      t.references :company, index: true

      t.timestamps null: false
    end
  end
end
