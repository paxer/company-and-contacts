class Contact < ActiveRecord::Base
  belongs_to :company
  has_many :friendships
  has_many :friends, through: :friendships

  validates :name, presence: true

  def friend_with?(contact)
    friendships.where(friend_id: contact.id).present?
  end
end