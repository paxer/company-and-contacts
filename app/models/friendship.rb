class Friendship < ActiveRecord::Base
  belongs_to :contact
  belongs_to :friend, class_name: 'Contact'

  validate :friendship_can_only_be_within_a_company

  private

  def friendship_can_only_be_within_a_company
    if contact.company != friend.company
      errors[:base] << 'A friendship can not be established because the contact belongs to the different company'
    end
  end
end